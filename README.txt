
* Compilation: run 'make fglm' in src/

* Generating test files: choose a magma test file from examples/magma_inputs, 
  say filename.mgm, and run 
    magma magma_inputs/filename.mgm solve.mgm
  from the directory examples/ This will generate a .dat and a .sage file in data/
  Do not move them.   The .dat contains the multiplication matrices. 

  Special case of files called random-....mgm. These files require extra 
  arguments. Choose an integer m and run one of
    magma d:=m magma_inputs/random-3-vars.mgm solve.mgm 
    magma d:=m magma_inputs/random-4-vars.mgm solve.mgm
    magma d:=m magma_inputs/random-3-vars-mixed.mgm solve.mgm
    magma d:=m magma_inputs/random-4-vars-mixed.mgm solve.mgm
    magma n:=m magma_inputs/random-quadratic.mgm solve.mgm
    magma n:=m magma_inputs/random-quadratic-mixed.mgm solve.mgm
  Or choose integers a,b,c and run on of
    magma D:=a n:=b p:=c magma_inputs/random-W1.mgm solve.mgm
    magma D:=a n:=b p:=c magma_inputs/random-W1-mixed.mgm solve.mgm

* Testing: from src/, run
  ./fglm -M num_threads -F ../examples/data/filename_dat -t thresh
  will run using num_threads threads, and pm-basis threshold thresh
  input filename.dat
 
  Run sage < filename_sol.sage will test the solution (by using the 
  .sage file generated before)
